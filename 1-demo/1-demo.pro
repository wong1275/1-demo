#-------------------------------------------------
#
# Project created by QtCreator 2014-12-21T23:38:24
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 1-demo
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp

HEADERS  += widget.h
